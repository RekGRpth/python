FROM rekgrpth/gost

MAINTAINER RekGRpth

RUN apk update --no-cache \
    && apk upgrade --no-cache \
    && apk add --no-cache --virtual .build-deps \
        bzip2-dev \
        expat-dev \
        gcc \
        gdbm-dev \
        !gettext-dev \
        git \
        libffi-dev \
        linux-headers \
        make \
        musl-dev \
        ncurses-dev \
        openssl-dev \
        readline-dev \
        sqlite-dev \
        tcl-dev \
        xz-dev \
        zlib-dev \
    && mkdir -p /usr/src \
    && cd /usr/src \
    && git clone --recursive https://github.com/RekGRpth/cpython.git \
    && cd /usr/src/cpython \
    && git checkout --track origin/3.7 \
    && ./configure \
        --enable-ipv6 \
        --enable-loadable-sqlite-extensions \
        --enable-shared \
        --with-computed-gotos \
        --with-dbmliborder=gdbm:ndbm \
        --with-ensurepip=upgrade \
        --with-lto \
        --with-system-expat \
        --with-system-ffi \
    && make -j"$(nproc)" \
    && make -j"$(nproc)" install \
    && cd / \
    && ln -s idle3 /usr/local/bin/idle \
    && ln -s pip3 /usr/local/bin/pip \
    && ln -s pydoc3 /usr/local/bin/pydoc \
    && ln -s python3 /usr/local/bin/python \
    && ln -s python3-config /usr/local/bin/python-config \
    && apk add --no-cache --virtual .python-rundeps \
        $( scanelf --needed --nobanner --format '%n#p' --recursive /usr/local \
            | tr ',' '\n' \
            | sort -u \
            | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
        ) \
    && apk del --no-cache .build-deps \
    && find /usr/local -depth \( \( -type d -a \( -name test -o -name tests \) \) \) -exec rm -rf '{}' + \
    && rm -rf /usr/src

CMD [ "python" ]
